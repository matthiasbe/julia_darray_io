# Julia DistributedArrays Input/Ouput

These few helper function enable to store an distributed array from RAM to the disk, and reload it from disk to RAM.

Distributed arrays are in the form of DistributedArrays.jl's `DArray` type.

It uses also the `Distributed.jl` messaging library.

## Usage

* Store to disk with `write_darray()`
* Load from disk with `read_darray()`


